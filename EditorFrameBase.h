#ifndef EDITORFRAMEBASE_H
#define EDITORFRAMEBASE_H

#include <wx/frame.h>

class EditorFrameBase : public wxFrame
{
public:
  EditorFrameBase(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxEmptyString, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE);
  virtual ~EditorFrameBase();

protected:
  virtual void OnNewFile(wxCommandEvent& event) { event.Skip(); }
  virtual void OnOpenFile(wxCommandEvent& event) { event.Skip(); }
  virtual void OnSaveFile(wxCommandEvent& event) { event.Skip(); }
  virtual void OnSaveAsFile(wxCommandEvent& event) { event.Skip(); }
  virtual void OnCloseFile(wxCommandEvent& event) { event.Skip(); }
  virtual void OnExit(wxCommandEvent& event) { event.Skip(); }
  virtual void OnAbout(wxCommandEvent& event) { event.Skip(); }

protected:
  wxMenuBar* m_menuBar;
  wxMenu* m_fileMenu;
  wxMenuItem* m_newMenuItem;
  wxMenuItem* m_openMenuItem;
  wxMenuItem* m_saveMenuItem;
  wxMenuItem* m_saveAsMenuItem;
  wxMenuItem* m_closeMenuItem;
  wxMenuItem* m_exitMenuItem;
  wxMenu* m_helpMenu;
  wxMenuItem* m_aboutMenuItem;
  wxStatusBar* m_statusBar;
};

#endif // EDITORFRAMEBASE_H

