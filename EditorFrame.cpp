#include "EditorFrame.h"
#include <wx/msgdlg.h>

EditorFrame::EditorFrame(wxWindow* parent)
  : EditorFrameBase(parent, wxID_ANY, _("Simple Editor"))
{
  SetStatusText(_("Welcome to Simple Editor!"));
  
  SetSizeHints(wxSize(640, 480));

  Centre(wxBOTH);
}

EditorFrame::~EditorFrame()
{
}

void EditorFrame::OnExit(wxCommandEvent& event)
{
  Close(true);
}

void EditorFrame::OnAbout(wxCommandEvent& event)
{
	wxMessageBox("About Simple Editor", "Simple Editor", wxICON_INFORMATION | wxOK);
}

