#include "EditorFrameBase.h"
#include <wx/menu.h>
#include <wx/sizer.h>
#include <wx/panel.h>

EditorFrameBase::EditorFrameBase(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style)
  : wxFrame(parent, id, title, pos, size, style)
{
  m_menuBar = new wxMenuBar();
  SetMenuBar(m_menuBar);

  m_fileMenu = new wxMenu();
  m_menuBar->Append(m_fileMenu, _("&File"));

  m_newMenuItem = new wxMenuItem(m_fileMenu, wxID_NEW);
  m_fileMenu->Append(m_newMenuItem);

  m_openMenuItem = new wxMenuItem(m_fileMenu, wxID_OPEN);
  m_fileMenu->Append(m_openMenuItem);

  m_saveMenuItem = new wxMenuItem(m_fileMenu, wxID_SAVE);
  m_fileMenu->Append(m_saveMenuItem);

  m_saveAsMenuItem = new wxMenuItem(m_fileMenu, wxID_SAVEAS);
  m_fileMenu->Append(m_saveAsMenuItem);

  m_closeMenuItem = new wxMenuItem(m_fileMenu, wxID_CLOSE);
  m_fileMenu->Append(m_closeMenuItem);

  m_fileMenu->AppendSeparator();

  m_exitMenuItem = new wxMenuItem(m_fileMenu, wxID_EXIT);
  m_fileMenu->Append(m_exitMenuItem);

  m_helpMenu = new wxMenu();
  m_menuBar->Append(m_helpMenu, _("&Help"));

  m_aboutMenuItem = new wxMenuItem(m_helpMenu, wxID_ABOUT);
  m_helpMenu->Append(m_aboutMenuItem);

  m_statusBar = CreateStatusBar();
  SetStatusBar(m_statusBar);

  wxBoxSizer* boxSizer = new wxBoxSizer(wxVERTICAL);
  SetSizer(boxSizer);

  wxPanel* panel = new wxPanel(this);
  boxSizer->Add(panel, 1, wxALL|wxEXPAND, 0);

  Connect(wxID_NEW, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(EditorFrameBase::OnNewFile));
  Connect(wxID_OPEN, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(EditorFrameBase::OnOpenFile));
  Connect(wxID_SAVE, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(EditorFrameBase::OnSaveFile));
  Connect(wxID_SAVEAS, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(EditorFrameBase::OnSaveAsFile));
  Connect(wxID_CLOSE, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(EditorFrameBase::OnCloseFile));
  Connect(wxID_EXIT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(EditorFrameBase::OnExit));
  Connect(wxID_ABOUT, wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(EditorFrameBase::OnAbout));
}

EditorFrameBase::~EditorFrameBase()
{
}

