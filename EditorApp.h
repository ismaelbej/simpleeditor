#ifndef EDITORAPP_H
#define EDITORAPP_H

#include <wx/app.h>

class EditorApp : public wxApp
{
public:
  EditorApp();
  virtual ~EditorApp();

  virtual bool OnInit();
};

DECLARE_APP(EditorApp);

#endif // EDITORAPP_H

