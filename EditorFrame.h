#ifndef EDITORFRAME_H
#define EDITORFRAME_H

#include "EditorFrameBase.h"

class EditorFrame : public EditorFrameBase
{
public:
  EditorFrame(wxWindow* parent);
  virtual ~EditorFrame();

protected:
  virtual void OnExit(wxCommandEvent& event);
  virtual void OnAbout(wxCommandEvent& event);
};

#endif // EIDTORFRAME_H

