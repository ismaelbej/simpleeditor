#include "EditorApp.h"
#include "EditorFrame.h"

EditorApp::EditorApp()
{
}

EditorApp::~EditorApp()
{
}

bool EditorApp::OnInit()
{
  EditorFrame* pFrame = new EditorFrame(NULL);
  SetTopWindow(pFrame);
  return pFrame->Show();
}

IMPLEMENT_APP(EditorApp);

